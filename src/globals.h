#if !defined(GLOBALS_h)
#define GLOBALS_h
#include "settings.h"

#define LED_BUILTIN 2

#define SKETCH_NAME "Base"
#define SKETCH_VERSION "1.0.0"

#endif // GLOBALS_h
