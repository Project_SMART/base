#if !defined(OTA_h)
#define OTA_h
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

void setupOTA(const char* chipName, const char* password);
void handleOTA();

#endif // OTA_h
