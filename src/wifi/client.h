#if !defined(WIFI_CLIENT_h)
#define WIFI_CLIENT_h

#include <ESP8266WiFi.h>

bool connectToWiFi(const char* ssid, const char* password, int timeoutSec);

#endif // WIFI_CLIENT_h
