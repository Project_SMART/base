#include "client.h"

bool connectToWiFi(const char* ssid, const char* password, int timeoutSec) {
  Serial.printf("Connecting to WiFi: SSID=%s\n", ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  int time=0;
  wl_status_t status;
  while ((status = WiFi.status()) == WL_DISCONNECTED) {
    delay(500);
    time++;
    if (time >= timeoutSec*2) {
      status = WiFi.status();
      break;
    }
  }

  switch (status) {
    case WL_CONNECTED:
      Serial.println("WiFi connected successfully");
      return true;
    case WL_DISCONNECTED:
      Serial.println("WiFi connection timeout");
      break;
    case WL_NO_SSID_AVAIL:
      Serial.println("WiFi AP is not found");
      break;
    default:
      Serial.println("WiFi connection failed");
      break;
  }
  return false;
}
