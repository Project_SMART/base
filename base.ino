#include <Arduino.h>

#include "src/globals.h"
#include "src/wifi/client.h"
#include "src/wifi/ota.h"

/**
 * Connects to WiFi 
 **/
void interactiveWiFiSetup() {
  digitalWrite(LED_BUILTIN, LOW);
  bool result = connectToWiFi(WIFI_SSID, WIFI_PASS, 25);
  digitalWrite(LED_BUILTIN, HIGH);

  if (!result) {
    for (int i=0; i<10; i++) {
      digitalWrite(LED_BUILTIN, LOW); //enable
      delay(500);
      digitalWrite(LED_BUILTIN, HIGH); // disable
      delay(500);
    }
    ESP.restart();
  }
}

void setup() {
  Serial.begin(115200);
  Serial.printf("\nLoading SMART sketch: %s (version %s)\n", SKETCH_NAME, SKETCH_VERSION);

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  interactiveWiFiSetup();
  setupOTA(CHIP_NAME, CHIP_OTA_PASS);
}

int ledI = 0;
int ledState = 0;
void loop() {
  handleOTA();

  if (ledI >= 0 && ledI <= 3) {
    if (ledState == 0) {
      digitalWrite(LED_BUILTIN, LOW);
      ledState=1;
    }
  } else if (ledState == 1) {
    digitalWrite(LED_BUILTIN, HIGH);
    ledState=0;
  }
  ledI++;
  if (ledI > 53) ledI = 0;
  delay(100);
}
